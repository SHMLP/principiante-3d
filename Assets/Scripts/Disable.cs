using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Disable : MonoBehaviour
{
    public TextMeshProUGUI turnOff;
    public float duracionTiempoDeshabilitado;
    

    float tiempoDeshabilitado;
    bool deshabilitado;
    Material gargoyleLight;
    CapsuleCollider gargoyleCollider;
    public GameEnding gameEnding;
    
    LineRenderer circle;
    private void OnTriggerStay(Collider other)
    {
        if (other.tag=="Gargoyle" && gameEnding.GetCaughtPlayer()==false)
        {
            turnOff.gameObject.SetActive(true);
            if (Input.GetButtonDown("Jump") && deshabilitado==false)
            {
                deshabilitado = true;  
                gargoyleLight = other.transform.Find("GargoyleModel").GetComponent<SkinnedMeshRenderer>().materials[0];
                gargoyleCollider = other.transform.Find("PointOfView").GetComponent<CapsuleCollider>();
                gargoyleLight.DisableKeyword("_EMISSION");
                gargoyleCollider.enabled = false;
            }
        }
        else if (gameEnding.GetCaughtPlayer() == true)
        {
            turnOff.gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        turnOff.gameObject.SetActive(false);
    }

    void Update()
    {
        if (deshabilitado)
        {
            tiempoDeshabilitado += Time.deltaTime;
            if (tiempoDeshabilitado>=duracionTiempoDeshabilitado)
            {
                gargoyleLight.EnableKeyword("_EMISSION");
                gargoyleCollider.enabled = true;
                gargoyleLight = null;
                gargoyleCollider = null;
                deshabilitado = false;
                tiempoDeshabilitado = 0;
            }
        }
    }
}
