using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vanish : MonoBehaviour
{
    public float duracionTiempoDesactivacion,duracionParedesDesactivadas;
    float tiempoDesactivacion;
    public GameObject[] paredes;
    public Material ghostWall, ghostDoor;

    static int contador;
    public bool[] activar = new bool[2];
    public GameObject disable;

    private void Start()
    {
        contador = 0;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            GetComponent<CapsuleCollider>().enabled = false;
            gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            activar[contador] = true;
            if (activar[1])
                disable.SetActive(true);
            contador += 1;
        }
    }
    void Update()
    {
        if (activar[0])
        {
            tiempoDesactivacion += Time.deltaTime;
            if (tiempoDesactivacion>=duracionTiempoDesactivacion)
            {
                StartCoroutine(ParedesDesactivadas(false));
                StartCoroutine(ParedesDesactivadas(true));
                tiempoDesactivacion = 0;
            }
            
        }
    }

    IEnumerator ParedesDesactivadas(bool Desaparecer)
    {
        if (Desaparecer)
        {
            yield return new WaitForSeconds(duracionParedesDesactivadas);
        }
        foreach (GameObject item in paredes)
        {
            item.GetComponent<BoxCollider>().enabled = Desaparecer;
            if (Desaparecer)
            {
                ghostWall.DisableKeyword("_EMISSION");
                ghostDoor.DisableKeyword("_EMISSION");
            }
            else
            {
                ghostWall.EnableKeyword("_EMISSION");
                ghostDoor.EnableKeyword("_EMISSION");
            }
        }
    }
}
